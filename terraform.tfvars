#Infrastructure values
aws_vpc_cidr_block = "10.0.0.0/16"
aws_subnets_count  = 3

#ECS values
aws_ecs_cluster_name           = "drover"
aws_ecs_task_definition_family = "drover-task"
aws_ecs_task_definition_cpu    = 256
aws_ecs_task_definition_memory = 512
aws_ecs_service_desired        = 1
aws_ecs_service_container_name = "nginx"
aws_ecs_application_port       = 80
aws_autoscaling_max            = 5
aws_autoscaling_min            = 1
aws_autoscaling_cpu_average    = 60
aws_autoscaling_memory_average = 80