output "aws_vpc_id" {
  description = "AWS VPC id"
  value       = aws_vpc.main.*.id
}

output "aws_subnet_id_private" {
  description = "AWS private subnet ids"
  value       = aws_subnet.private.*.id
}

output "aws_subnet_id_public" {
  description = "AWS public subnet ids"
  value       = aws_subnet.public.*.id
}

output "aws_lb_target_group_id" {
  description = "Loadbalancer target group arn"
  value       = aws_lb_target_group.app.id
}

output "aws_security_group_id" {
  description = "Backend security group id"
  value       = aws_security_group.backend.id
}

output "aws_lb_dns" {
  description = "DNS of loadbalancer"
  value       = aws_lb.main.dns_name
}