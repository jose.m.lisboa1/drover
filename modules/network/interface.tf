variable "aws_vpc_cidr_block" {
  type        = string
  description = "VPC CIDR block"
}

variable "aws_subnets_count" {
  type        = number
  description = "Number os subnets to create"
}

variable "aws_ecs_application_port" {
  type        = number
  description = "Application internal port"
}