variable "aws_ecs_cluster_name" {
  type        = string
  description = "Name of ECS cluster"
}

variable "aws_ecs_task_definition_family" {
  type        = string
  description = "Task definition family"
}

variable "aws_ecs_task_definition_cpu" {
  type        = number
  description = "The number of cpu units used by the task"
}

variable "aws_ecs_task_definition_memory" {
  type        = number
  description = "The amount (in MiB) of memory used by the task"
}

variable "aws_ecs_service_desired" {
  type        = number
  description = "The number os desired services running"
}

variable "aws_ecs_service_subnets" {
  description = "List of subnets to use in ecs"
}

variable "aws_ecs_service_target_group_arn" {
  type        = string
  description = "Add target group to loadbalancer"
}

variable "aws_ecs_service_container_name" {
  type        = string
  description = "ECS service container name"
}

variable "aws_ecs_service_container_port" {
  type        = number
  description = "ECS service container name port"
}

variable "aws_ecs_service_security_groups" {
  description = "ECS security group"
}

variable "aws_autoscaling_max" {
  type = number
  description = "Maximum number of services with autoscaling"
}

variable "aws_autoscaling_min" {
  type = number
  description = "Minimum number of services with autoscaling"
}

variable "aws_autoscaling_memory_average" {
  type = number
  description = "Average memory to autoscale services"
}

variable "aws_autoscaling_cpu_average" {
  type = number
  description = "Average cpu to autoscale services"
}