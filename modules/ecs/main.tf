resource "aws_ecs_cluster" "main" {
  name = var.aws_ecs_cluster_name
}

resource "aws_ecs_task_definition" "main" {
  family                   = var.aws_ecs_task_definition_family
  container_definitions    = file("service.json")
  cpu                      = var.aws_ecs_task_definition_cpu
  memory                   = var.aws_ecs_task_definition_memory
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
}

resource "aws_ecs_service" "main" {
  name            = "drover-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.main.arn
  launch_type     = "FARGATE"
  desired_count   = var.aws_ecs_service_desired

  network_configuration {
    subnets         = var.aws_ecs_service_subnets
    security_groups = var.aws_ecs_service_security_groups
  }

  load_balancer {
    target_group_arn = var.aws_ecs_service_target_group_arn
    container_name   = var.aws_ecs_service_container_name
    container_port   = var.aws_ecs_service_container_port
  }
}

##AutoScaling
resource "aws_appautoscaling_target" "main" {
  max_capacity = var.aws_autoscaling_max
  min_capacity = var.aws_autoscaling_min
  resource_id = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

resource "aws_appautoscaling_policy" "memory" {
  name               = "ecs-memory-policy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.main.resource_id
  scalable_dimension = aws_appautoscaling_target.main.scalable_dimension
  service_namespace  = aws_appautoscaling_target.main.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value = var.aws_autoscaling_memory_average
  }
}

resource "aws_appautoscaling_policy" "cpu" {
  name = "ecs-cpu-policy"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.main.resource_id
  scalable_dimension = aws_appautoscaling_target.main.scalable_dimension
  service_namespace = aws_appautoscaling_target.main.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = var.aws_autoscaling_cpu_average
  }
}