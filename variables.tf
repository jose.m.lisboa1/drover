variable "aws_region" {
  type        = string
  description = "(optional) describe your variable"
  default     = "eu-west-1"
}

variable "aws_vpc_cidr_block" {
  description = "VPC CIDR where application will be deployed"
}

variable "aws_subnets_count" {
  type        = number
  description = "Number of subnets to create(same number for private and public)"
}

variable "aws_ecs_application_port" {
  type        = number
  description = "Port exposed by the application"
}

variable "aws_ecs_cluster_name" {
  type        = string
  description = "ECS cluster name"
}

variable "aws_ecs_task_definition_family" {
  type        = string
  description = "ECS task definition"
}

variable "aws_ecs_task_definition_cpu" {
  type        = number
  description = "ECS task CPU"
}

variable "aws_ecs_task_definition_memory" {
  type        = number
  description = "ECS task memory"
}

variable "aws_ecs_service_desired" {
  type        = number
  description = "ECS number of desired tasks"
}

variable "aws_ecs_service_container_name" {
  type        = string
  description = "ECS container name"
}

variable "aws_autoscaling_max" {
  type        = number
  description = "ECS autoscaling max tasks"
}

variable "aws_autoscaling_min" {
  type        = number
  description = "ECS autoscaling min tasks"
}

variable "aws_autoscaling_cpu_average" {
  type        = number
  description = "ECS autoscaling average percentage CPU"
}

variable "aws_autoscaling_memory_average" {
  type        = number
  description = "ECS autoscaling average percentage MEMORY "
}