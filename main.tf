terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

module "infrastructure" {
  source                   = "./modules/network"
  aws_vpc_cidr_block       = var.aws_vpc_cidr_block
  aws_subnets_count        = var.aws_subnets_count
  aws_ecs_application_port = var.aws_ecs_application_port
}

module "drover-app" {
  source                           = "./modules/ecs"
  aws_ecs_cluster_name             = var.aws_ecs_cluster_name
  aws_ecs_task_definition_family   = var.aws_ecs_task_definition_family
  aws_ecs_task_definition_cpu      = var.aws_ecs_task_definition_cpu
  aws_ecs_task_definition_memory   = var.aws_ecs_task_definition_memory
  aws_ecs_service_desired          = var.aws_ecs_service_desired
  aws_ecs_service_subnets          = module.infrastructure.aws_subnet_id_private
  aws_ecs_service_security_groups  = [module.infrastructure.aws_security_group_id]
  aws_ecs_service_target_group_arn = module.infrastructure.aws_lb_target_group_id
  aws_ecs_service_container_name   = var.aws_ecs_service_container_name
  aws_ecs_service_container_port   = var.aws_ecs_application_port
  aws_autoscaling_max              = var.aws_autoscaling_max
  aws_autoscaling_min              = var.aws_autoscaling_min
  aws_autoscaling_cpu_average      = var.aws_autoscaling_cpu_average
  aws_autoscaling_memory_average   = var.aws_autoscaling_memory_average
}