
output "load_balancer_dns" {
  description = "DNS of loadbalancer"
  value       = module.infrastructure.aws_lb_dns
}